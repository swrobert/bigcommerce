<?php
namespace App\Repositories\Orders;


use Illuminate\Support\ServiceProvider;


class OrdersRepoServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Orders\OrdersRepositoryInterface', 'App\Repositories\Orders\OrdersRepository');
    }
}