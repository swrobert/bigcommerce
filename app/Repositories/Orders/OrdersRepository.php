<?php namespace App\Repositories\Orders;

use App\Orders;

class OrdersRepository implements OrdersRepositoryInterface
{
    // model property on class instances
    protected $model;
    protected $customer_model;

    // Constructor to bind model to repo
    public function __construct(Orders $model)
    {
        $this->model = $model;

    }

    // Get all instances of model
    public function all()
    {
        return $this->model->getAll();
    }

    // Find orders by customer id
    public function find($id)
    {
        return $this->model->getOrdersById($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

}