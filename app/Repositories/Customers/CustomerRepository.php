<?php namespace App\Repositories\Customers;

use App\Customers;

class CustomerRepository implements CustomerRepositoryInterface
{
    // model property on class instances
    protected $model;

    // Constructor to bind model to repo
    public function __construct(Customers $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all()
    {
        return $this->model->getAll();
    }

    /**
     * Get customers
     *
     * @param $id
     * @return int
     */
    public function find($id)
    {
        return $this->model->getTotalOrders($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

}