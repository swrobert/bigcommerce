<?php
namespace App\Repositories\Customers;


use Illuminate\Support\ServiceProvider;


class CustomerRepoServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Customer\CustomerRepositoryInterface', 'App\Repositories\Customer\CustomerRepository');
    }
}