<?php

namespace App;

use Bigcommerce\Api\Client as Bigcommerce;

class Customers extends Bigcommerce
{

    public $first_name = null;
    public $last_name = null;

    /**
     * Get All Customers
     *
     * @return array
     */
    public function getAll()
    {
        $customers = static::getCustomers();

        return $customers;
    }

    /**
     * Get Customer Count
     *
     * @return int
     */
    public function count()
    {
        return Bigcommerce::getCustomersCount();
    }

    /**
     * Get total orders placed by customer
     *
     * @param $id
     * @return int
     */
    public function getTotalOrders($id)
    {
        $order = new Orders();
        return $order->getTotalOrders($id);
    }

    public function getNameById($id)
    {
        $customer = Bigcommerce::getCustomer((int)$id);
        $this->first_name = $customer->first_name;
        $this->last_name = $customer->last_name;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

}