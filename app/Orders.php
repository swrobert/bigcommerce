<?php

namespace App;

use Bigcommerce\Api\Client as Bigcommerce;

class Orders
{
    public $created_date = null;

    /**
     * Get All
     *
     * @return array
     */
    public function getAll()
    {
        $orders = Bigcommerce::getOrders();

        return $orders;
    }

    /**
     * Get Count
     *
     * @return int
     */
    public function count()
    {
        return Bigcommerce::getOrdersCount();
    }

    /**
     * Get Order By Customer Id
     *
     * @param $id
     * @return array
     */
    public function getOrdersById($id)
    {
        $filter = ['customer_id' => $id];
        return Bigcommerce::getOrders($filter);
    }

    /**
     * Get Total Orders by Customer ID
     *
     * @param $id
     * @return int
     */
    public function getTotalOrders($id)
    {
        $filter = ['customer_id' => $id];
        return Bigcommerce::getOrdersCount($filter);
    }
}