<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Repositories\Orders\OrdersRepository;
use App\Repositories\Customers\CustomerRepository;
use App\Customers;

class CustomerDetailsController extends BaseController
{
    protected $model;
    protected $customer_model;

    public function __construct(OrdersRepository $orders, CustomerRepository $customers)
    {
        $this->model = $orders;
        $this->customer_model = $customers;
    }

    public function show($id)
    {
        $orders = $this->model->find($id);

        $life_time_value = 0;

        foreach ($orders as $index => $order) {
            $life_time_value = $life_time_value + ($order->items_total * $order->total_inc_tax);
        }

        $model = new Customers();
        $model->getNameById($id);

        $customer = new \stdClass();
        $customer->first_name = $model->getFirstName();
        $customer->last_name = $model->getFirstName();

        return view('details', [
            'customer' => $customer,
            'lifeTimeValue' => $life_time_value,
            'orders' => $orders,
        ]);
    }
}
