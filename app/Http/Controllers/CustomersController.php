<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Repositories\Customers\CustomerRepository;

class CustomersController extends BaseController
{
    protected $model;

    public function __construct(CustomerRepository $customers)
    {
        $this->model = $customers;
    }

    /**
     * List Customers
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $customers = $this->model->all();
        $data = [];
        foreach ($customers as $index => $row) {
            $data[$index]['id'] = $row->id;
            $data[$index]['name'] = $row->first_name . ' ' . $row->last_name;
            $data[$index]['count'] = $this->model->find($row->id);
        }

        return view('customers', compact('data'));
    }
}
