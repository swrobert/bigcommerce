@extends('layouts.app')

@section('title', 'Customers')

@section('content')
    <table>
        <thead>
        <tr>
            <th>Name</th>
            <th># of Orders</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $index=>$customer)
            <tr>
                <td><a href="{{ url('customers', $customer['id']) }}" class="btn btn-info">
                        {{ $customer['name'] }}
                    </a>
                </td>
                <td>{{ $customer['count'] }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
