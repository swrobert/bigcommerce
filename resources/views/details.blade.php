@extends('layouts.app')

@section('title', $customer->first_name . "'s Order History")

@section('content')
    <table>
        <thead>
        <tr>
            <th>Date</th>
            <th># of Products</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($orders as $index=>$order)
            <tr>
                <td>{{ $order->date_created }}</td>
                <td>{{ $order->items_total }}</td>
                <td>${{ $order->items_total * $order->total_inc_tax }}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="2">Lifetime Value</td>
            <td>${{ $lifeTimeValue }}</td>
        </tr>
        </tbody>
    </table>
@endsection
